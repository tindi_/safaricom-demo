# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Safaricom Technical Interview App 'Twaweza'
* Version 0.0.1

### What is Twaweza about ###

* The app utilizes public API's from https://github.com/abhishekbanthia/Public-APIs
* Specifically:
* Firebase-auth for Authentication
* Google Places API for Location
* Uber Rides API for Transportation
* The user creates an account utilizing the Firebase Auth API
* The users location is retrieved using Google Places API
* The user can book a cab using Uber Rides API

### Tests and Instrumentation ###
* Unit tests done using Roboelectric within the app

### Threading ###
* Done using AsyncTask within particular libraries

### Who do I talk to? ###

* Njoro Kitindi njoro.kitindi@gmail.com