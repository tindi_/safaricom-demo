package com.saf.demo;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.email)
    EditText txtMail;

    @Bind(R.id.password)
    EditText txtPass;

    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Check if the user is logged in
        //Firebase auth instance
        auth = FirebaseAuth.getInstance();

        if (auth.getCurrentUser() != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        //Firebase auth instance
        auth = FirebaseAuth.getInstance();

    }

    @OnClick(R.id.btn_login)
    public void onClick(View view) {
        attemptLogin();
    }

    @OnClick(R.id.btn_signup)
    void register(View view) {
        startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
    }

    private void attemptLogin() {
        if (isValid()) {

            String email = txtMail.getText().toString();
            final String password = txtPass.getText().toString();

            progressBar.setVisibility(View.VISIBLE);

            //authenticate user with Firebase
            auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            progressBar.setVisibility(View.GONE);

                            if (!task.isSuccessful()) {

                                if (password.length() < 6) {
                                    txtPass.setError(getString(R.string.minimum_password));
                                } else {
                                    Toast.makeText(LoginActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }

                        }
                    });

        }
    }

    boolean isValid() {

        if (txtMail.getText().toString().isEmpty()) {
            txtMail.setError("E-Mail is required");
            txtMail.requestFocus();
            return false;
        }

        if (txtPass.getText().toString().isEmpty()) {
            txtPass.setError("Password is required");
            txtPass.requestFocus();
            return false;
        }
        return true;

    }
}
