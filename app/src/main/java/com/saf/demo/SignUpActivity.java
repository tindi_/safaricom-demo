package com.saf.demo;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity {

    private FirebaseAuth auth;

    @Bind(R.id.tool_bar)
    Toolbar toolBar;

    @Bind(R.id.extMail)
    EditText txtMail;

    @Bind(R.id.extPass)
    EditText txtPass;

    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        //Firebase auth instance
        auth = FirebaseAuth.getInstance();

        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @OnClick(R.id.btn_signup)
    void register(View view) {
        if (isValid()) {

            String mail = txtMail.getText().toString();
            String pass = txtPass.getText().toString();

            progressBar.setVisibility(View.VISIBLE);

            auth.createUserWithEmailAndPassword(mail, pass)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            progressBar.setVisibility(View.GONE);

                            if (!task.isSuccessful()) {
                                Toast.makeText(SignUpActivity.this, "Authentication failed." + task.getException(),
                                        Toast.LENGTH_SHORT).show();
                            } else {

                                alert("Success", "Your account has been created successfully.", true);

                            }
                        }
                    });
        }

    }

    boolean isValid() {

        if (txtMail.getText().toString().isEmpty()) {
            txtMail.setError("E-Mail is required");
            txtMail.requestFocus();
            return false;
        }

        if (txtPass.getText().toString().isEmpty()) {
            txtPass.setError("Password is required");
            txtPass.requestFocus();
            return false;
        }

        if (txtPass.getText().toString().length() < 5) {
            txtPass.setError("Invalid password, should be longer than 5 characters");
            txtPass.requestFocus();
            return false;
        }


        return true;

    }

    void alert(String title, String message, final boolean finish) {
        new AlertDialog.Builder(SignUpActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (finish) {
                            finish();
                        }
                    }
                }).create().show();
    }


}
